VERSION=v2.3.5

gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION)
	go mod tidy
